import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

export class ModalServiceStub {
    constructor() {
    }

    _resultValue!: string;

    set resultValue(value: string) {
        this._resultValue = value;
    }

    open() {
        return {
            result: {
                then: (result: (result: string) => void, reason: () => void) => {
                    result(this._resultValue);
                    reason();
                },
            },
        };
    }
}
