import {CopyToClipboardOptions} from '@common/models';
import {UtilityService} from '@common/services';
import AOS, {Aos} from 'aos';
import {of} from 'rxjs';

export class UtilityServiceStub implements Partial<UtilityService> {
    version$ = of('VERSION');
    AOS = {
        init: () => {
        },
    } as Aos;

    _window = window;

    get window() {
        return this._window;
    }

    getStoredObject = <T>(objectName: string) => undefined;
    storeObject = (objectName: string, objectToStore: {}) => {
    };
    copyToClipboard = (text: string, options?: CopyToClipboardOptions) => {
    };
}
